require 'test_helper'

class AccessTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @access_type = access_types(:one)
  end

  test "should get index" do
    get access_types_url
    assert_response :success
  end

  test "should get new" do
    get new_access_type_url
    assert_response :success
  end

  test "should create access_type" do
    assert_difference('AccessType.count') do
      post access_types_url, params: { access_type: { moving_type: @access_type.moving_type } }
    end

    assert_redirected_to access_type_url(AccessType.last)
  end

  test "should show access_type" do
    get access_type_url(@access_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_access_type_url(@access_type)
    assert_response :success
  end

  test "should update access_type" do
    patch access_type_url(@access_type), params: { access_type: { moving_type: @access_type.moving_type } }
    assert_redirected_to access_type_url(@access_type)
  end

  test "should destroy access_type" do
    assert_difference('AccessType.count', -1) do
      delete access_type_url(@access_type)
    end

    assert_redirected_to access_types_url
  end
end
