require 'test_helper'

class MovingRoutesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @moving_route = moving_routes(:one)
  end

  test "should get index" do
    get moving_routes_url
    assert_response :success
  end

  test "should get new" do
    get new_moving_route_url
    assert_response :success
  end

  test "should create moving_route" do
    assert_difference('MovingRoute.count') do
      post moving_routes_url, params: { moving_route: { dept_time: @moving_route.dept_time, dest_time: @moving_route.dest_time, moving_time_id: @moving_route.moving_time_id, point: @moving_route.point, travel_hour: @moving_route.travel_hour } }
    end

    assert_redirected_to moving_route_url(MovingRoute.last)
  end

  test "should show moving_route" do
    get moving_route_url(@moving_route)
    assert_response :success
  end

  test "should get edit" do
    get edit_moving_route_url(@moving_route)
    assert_response :success
  end

  test "should update moving_route" do
    patch moving_route_url(@moving_route), params: { moving_route: { dept_time: @moving_route.dept_time, dest_time: @moving_route.dest_time, moving_time_id: @moving_route.moving_time_id, point: @moving_route.point, travel_hour: @moving_route.travel_hour } }
    assert_redirected_to moving_route_url(@moving_route)
  end

  test "should destroy moving_route" do
    assert_difference('MovingRoute.count', -1) do
      delete moving_route_url(@moving_route)
    end

    assert_redirected_to moving_routes_url
  end
end
