require 'test_helper'

class DailyAllowancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @daily_allowance = daily_allowances(:one)
  end

  test "should get index" do
    get daily_allowances_url
    assert_response :success
  end

  test "should get new" do
    get new_daily_allowance_url
    assert_response :success
  end

  test "should create daily_allowance" do
    assert_difference('DailyAllowance.count') do
      post daily_allowances_url, params: { daily_allowance: { fixed: @daily_allowance.fixed, fixed_over: @daily_allowance.fixed_over, holiday: @daily_allowance.holiday, moving_allowance: @daily_allowance.moving_allowance, report_id: @daily_allowance.report_id, trip_date: @daily_allowance.trip_date } }
    end

    assert_redirected_to daily_allowance_url(DailyAllowance.last)
  end

  test "should show daily_allowance" do
    get daily_allowance_url(@daily_allowance)
    assert_response :success
  end

  test "should get edit" do
    get edit_daily_allowance_url(@daily_allowance)
    assert_response :success
  end

  test "should update daily_allowance" do
    patch daily_allowance_url(@daily_allowance), params: { daily_allowance: { fixed: @daily_allowance.fixed, fixed_over: @daily_allowance.fixed_over, holiday: @daily_allowance.holiday, moving_allowance: @daily_allowance.moving_allowance, report_id: @daily_allowance.report_id, trip_date: @daily_allowance.trip_date } }
    assert_redirected_to daily_allowance_url(@daily_allowance)
  end

  test "should destroy daily_allowance" do
    assert_difference('DailyAllowance.count', -1) do
      delete daily_allowance_url(@daily_allowance)
    end

    assert_redirected_to daily_allowances_url
  end
end
