require 'test_helper'

class EtcListsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @etc_list = etc_lists(:one)
  end

  test "should get index" do
    get etc_lists_url
    assert_response :success
  end

  test "should get new" do
    get new_etc_list_url
    assert_response :success
  end

  test "should create etc_list" do
    assert_difference('EtcList.count') do
      post etc_lists_url, params: { etc_list: { name: @etc_list.name } }
    end

    assert_redirected_to etc_list_url(EtcList.last)
  end

  test "should show etc_list" do
    get etc_list_url(@etc_list)
    assert_response :success
  end

  test "should get edit" do
    get edit_etc_list_url(@etc_list)
    assert_response :success
  end

  test "should update etc_list" do
    patch etc_list_url(@etc_list), params: { etc_list: { name: @etc_list.name } }
    assert_redirected_to etc_list_url(@etc_list)
  end

  test "should destroy etc_list" do
    assert_difference('EtcList.count', -1) do
      delete etc_list_url(@etc_list)
    end

    assert_redirected_to etc_lists_url
  end
end
