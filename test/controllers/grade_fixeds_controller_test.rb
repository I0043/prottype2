require 'test_helper'

class GradeFixedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @grade_fixed = grade_fixeds(:one)
  end

  test "should get index" do
    get grade_fixeds_url
    assert_response :success
  end

  test "should get new" do
    get new_grade_fixed_url
    assert_response :success
  end

  test "should create grade_fixed" do
    assert_difference('GradeFixed.count') do
      post grade_fixeds_url, params: { grade_fixed: { change_date: @grade_fixed.change_date, fixed_id: @grade_fixed.fixed_id, grade_id: @grade_fixed.grade_id } }
    end

    assert_redirected_to grade_fixed_url(GradeFixed.last)
  end

  test "should show grade_fixed" do
    get grade_fixed_url(@grade_fixed)
    assert_response :success
  end

  test "should get edit" do
    get edit_grade_fixed_url(@grade_fixed)
    assert_response :success
  end

  test "should update grade_fixed" do
    patch grade_fixed_url(@grade_fixed), params: { grade_fixed: { change_date: @grade_fixed.change_date, fixed_id: @grade_fixed.fixed_id, grade_id: @grade_fixed.grade_id } }
    assert_redirected_to grade_fixed_url(@grade_fixed)
  end

  test "should destroy grade_fixed" do
    assert_difference('GradeFixed.count', -1) do
      delete grade_fixed_url(@grade_fixed)
    end

    assert_redirected_to grade_fixeds_url
  end
end
