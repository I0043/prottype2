require 'test_helper'

class GradelistsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @gradelist = gradelists(:one)
  end

  test "should get index" do
    get gradelists_url
    assert_response :success
  end

  test "should get new" do
    get new_gradelist_url
    assert_response :success
  end

  test "should create gradelist" do
    assert_difference('Gradelist.count') do
      post gradelists_url, params: { gradelist: { fixed: @gradelist.fixed, grade: @gradelist.grade } }
    end

    assert_redirected_to gradelist_url(Gradelist.last)
  end

  test "should show gradelist" do
    get gradelist_url(@gradelist)
    assert_response :success
  end

  test "should get edit" do
    get edit_gradelist_url(@gradelist)
    assert_response :success
  end

  test "should update gradelist" do
    patch gradelist_url(@gradelist), params: { gradelist: { fixed: @gradelist.fixed, grade: @gradelist.grade } }
    assert_redirected_to gradelist_url(@gradelist)
  end

  test "should destroy gradelist" do
    assert_difference('Gradelist.count', -1) do
      delete gradelist_url(@gradelist)
    end

    assert_redirected_to gradelists_url
  end
end
