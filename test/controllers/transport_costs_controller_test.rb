require 'test_helper'

class TransportCostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @transport_cost = transport_costs(:one)
  end

  test "should get index" do
    get transport_costs_url
    assert_response :success
  end

  test "should get new" do
    get new_transport_cost_url
    assert_response :success
  end

  test "should create transport_cost" do
    assert_difference('TransportCost.count') do
      post transport_costs_url, params: { transport_cost: { ETC: @transport_cost.ETC, access: @transport_cost.access, dept: @transport_cost.dept, dest: @transport_cost.dest, drive_type: @transport_cost.drive_type, move_date: @transport_cost.move_date, report_id: @transport_cost.report_id, root: @transport_cost.root, transport_cost: @transport_cost.transport_cost } }
    end

    assert_redirected_to transport_cost_url(TransportCost.last)
  end

  test "should show transport_cost" do
    get transport_cost_url(@transport_cost)
    assert_response :success
  end

  test "should get edit" do
    get edit_transport_cost_url(@transport_cost)
    assert_response :success
  end

  test "should update transport_cost" do
    patch transport_cost_url(@transport_cost), params: { transport_cost: { ETC: @transport_cost.ETC, access: @transport_cost.access, dept: @transport_cost.dept, dest: @transport_cost.dest, drive_type: @transport_cost.drive_type, move_date: @transport_cost.move_date, report_id: @transport_cost.report_id, root: @transport_cost.root, transport_cost: @transport_cost.transport_cost } }
    assert_redirected_to transport_cost_url(@transport_cost)
  end

  test "should destroy transport_cost" do
    assert_difference('TransportCost.count', -1) do
      delete transport_cost_url(@transport_cost)
    end

    assert_redirected_to transport_costs_url
  end
end
