require 'test_helper'

class MovingTimesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @moving_time = moving_times(:one)
  end

  test "should get index" do
    get moving_times_url
    assert_response :success
  end

  test "should get new" do
    get new_moving_time_url
    assert_response :success
  end

  test "should create moving_time" do
    assert_difference('MovingTime.count') do
      post moving_times_url, params: { moving_time: { dept_date: @moving_time.dept_date, dest_date: @moving_time.dest_date, moving_time: @moving_time.moving_time, place: @moving_time.place } }
    end

    assert_redirected_to moving_time_url(MovingTime.last)
  end

  test "should show moving_time" do
    get moving_time_url(@moving_time)
    assert_response :success
  end

  test "should get edit" do
    get edit_moving_time_url(@moving_time)
    assert_response :success
  end

  test "should update moving_time" do
    patch moving_time_url(@moving_time), params: { moving_time: { dept_date: @moving_time.dept_date, dest_date: @moving_time.dest_date, moving_time: @moving_time.moving_time, place: @moving_time.place } }
    assert_redirected_to moving_time_url(@moving_time)
  end

  test "should destroy moving_time" do
    assert_difference('MovingTime.count', -1) do
      delete moving_time_url(@moving_time)
    end

    assert_redirected_to moving_times_url
  end
end
