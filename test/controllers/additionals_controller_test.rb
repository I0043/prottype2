require 'test_helper'

class AdditionalsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @additional = additionals(:one)
  end

  test "should get index" do
    get additionals_url
    assert_response :success
  end

  test "should get new" do
    get new_additional_url
    assert_response :success
  end

  test "should create additional" do
    assert_difference('Additional.count') do
      post additionals_url, params: { additional: { additional: @additional.additional, additional_date: @additional.additional_date, dept_time: @additional.dept_time, dest_time: @additional.dest_time, moving_type: @additional.moving_type, report_id: @additional.report_id, vehicle_night: @additional.vehicle_night } }
    end

    assert_redirected_to additional_url(Additional.last)
  end

  test "should show additional" do
    get additional_url(@additional)
    assert_response :success
  end

  test "should get edit" do
    get edit_additional_url(@additional)
    assert_response :success
  end

  test "should update additional" do
    patch additional_url(@additional), params: { additional: { additional: @additional.additional, additional_date: @additional.additional_date, dept_time: @additional.dept_time, dest_time: @additional.dest_time, moving_type: @additional.moving_type, report_id: @additional.report_id, vehicle_night: @additional.vehicle_night } }
    assert_redirected_to additional_url(@additional)
  end

  test "should destroy additional" do
    assert_difference('Additional.count', -1) do
      delete additional_url(@additional)
    end

    assert_redirected_to additionals_url
  end
end
