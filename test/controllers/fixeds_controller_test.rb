require 'test_helper'

class FixedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @fixed = fixeds(:one)
  end

  test "should get index" do
    get fixeds_url
    assert_response :success
  end

  test "should get new" do
    get new_fixed_url
    assert_response :success
  end

  test "should create fixed" do
    assert_difference('Fixed.count') do
      post fixeds_url, params: { fixed: { fixed: @fixed.fixed, fixed_type: @fixed.fixed_type } }
    end

    assert_redirected_to fixed_url(Fixed.last)
  end

  test "should show fixed" do
    get fixed_url(@fixed)
    assert_response :success
  end

  test "should get edit" do
    get edit_fixed_url(@fixed)
    assert_response :success
  end

  test "should update fixed" do
    patch fixed_url(@fixed), params: { fixed: { fixed: @fixed.fixed, fixed_type: @fixed.fixed_type } }
    assert_redirected_to fixed_url(@fixed)
  end

  test "should destroy fixed" do
    assert_difference('Fixed.count', -1) do
      delete fixed_url(@fixed)
    end

    assert_redirected_to fixeds_url
  end
end
