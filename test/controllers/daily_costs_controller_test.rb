require 'test_helper'

class DailyCostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @daily_cost = daily_costs(:one)
  end

  test "should get index" do
    get daily_costs_url
    assert_response :success
  end

  test "should get new" do
    get new_daily_cost_url
    assert_response :success
  end

  test "should create daily_cost" do
    assert_difference('DailyCost.count') do
      post daily_costs_url, params: { daily_cost: { city_name: @daily_cost.city_name, course: @daily_cost.course, daily_cost: @daily_cost.daily_cost, daily_date: @daily_cost.daily_date, dept_time: @daily_cost.dept_time, dest_ime: @daily_cost.dest_ime, expenses_type: @daily_cost.expenses_type, report_id: @daily_cost.report_id, travel_hour: @daily_cost.travel_hour, work_type: @daily_cost.work_type } }
    end

    assert_redirected_to daily_cost_url(DailyCost.last)
  end

  test "should show daily_cost" do
    get daily_cost_url(@daily_cost)
    assert_response :success
  end

  test "should get edit" do
    get edit_daily_cost_url(@daily_cost)
    assert_response :success
  end

  test "should update daily_cost" do
    patch daily_cost_url(@daily_cost), params: { daily_cost: { city_name: @daily_cost.city_name, course: @daily_cost.course, daily_cost: @daily_cost.daily_cost, daily_date: @daily_cost.daily_date, dept_time: @daily_cost.dept_time, dest_ime: @daily_cost.dest_ime, expenses_type: @daily_cost.expenses_type, report_id: @daily_cost.report_id, travel_hour: @daily_cost.travel_hour, work_type: @daily_cost.work_type } }
    assert_redirected_to daily_cost_url(@daily_cost)
  end

  test "should destroy daily_cost" do
    assert_difference('DailyCost.count', -1) do
      delete daily_cost_url(@daily_cost)
    end

    assert_redirected_to daily_costs_url
  end
end
