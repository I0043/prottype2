require 'csv'

# rake import:users
namespace :import do
  desc "Import useers from csv"

  task calendars: :environment do
    path = File.join Rails.root, "db/sample.csv"
    puts "path: #{path}"
    list = []
    CSV.foreach(path, headers: true) do |row|
      list << {
          work_date: row["work_date"],
          holiday: row["holiday"]
      }
    end
    puts "start to create users data"
    begin
      User.create!(list)
      puts "completed!!"
    rescue ActiveModel::UnknownAttributeError => invalid
      puts "raised error : unKnown attribute "
    end
  end
end