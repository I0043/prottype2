class Report < ApplicationRecord
    belongs_to :order
    
    # has_many :accommodations
    has_many :transport_costs
    # has_many :daily_allowances
    # has_many :additionals
    has_many :others
    # has_many :moving_times
    has_many :daily_costs
end
