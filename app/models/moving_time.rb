class MovingTime < ApplicationRecord
    belongs_to :report
    
    has_many :moving_routes
end
