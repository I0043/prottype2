class Search::Order < Search::Base
  ATTRIBUTES = %i(
    udno
    skno
    product_name
    customer
  )
  attr_accessor(*ATTRIBUTES)

  def matches
    t = ::Order.arel_table
    results = ::Order.all
    results = results.where(contains(t[:udno], code)) if udno.present?
    results = results.where(contains(t[:skno], name)) if skno.present?
    results = results.where(contains(t[:product_name], name_kana)) if product_name.present?
    results = results.where(t[:customer].gteq(price_from)) if customer.present?
    results = results.where(availability: true) if value_to_boolean(availability)
    results
  end
end