class ReportsController < ApplicationController
  before_action :set_report, only: [:show, :edit, :update, :destroy]



  # GET /reports
  # GET /reports.json
  def index
    @reports = Report.all
    # logger.debug @reports.inspect
  end

  # GET /reports/1
  # GET /reports/1.json
  def show
    
  # @daily_allowances_total =  @report.daily_allowances.sum(:fixed) + @report.daily_allowances.sum(:fixed_over) + @report.daily_allowances.sum(:moving_allowance)
  # @accomodation_total = @report.accommodations.sum(:accommodation) + @report.accommodations.sum(:add_money)
  # @transport_cost_total =  @report.transport_costs.sum(:transport_cost)
  # @other_total = @report.others.sum(:other_money)
  # # @etc = @report.transport_costs.where(exclusion_type: 'etc')
  # # @etc_total = @etc.sum(:transport_cost)
  # # @airticket = @report.transport_costs.where(exclusion_type: '航空券')
  # # @airticket_total = @airticket.sum(:transport_cost)
  # @adjusted_amount = @daily_allowances_total + @accomodation_total + @transport_cost_total + @other_total
  
  # if @adjusted_amount > @report.tmp_money + @etc_total + @airticket_total
  #     @add_money = @adjusted_amount - @report.tmp_money - @etc_total - @airticket_total
  #   else
  #     @return_money = @report.tmp_money + @etc_total + @airticket_total - @adjusted_amount
  # end
  
  # 出発時刻がdatetime型の為、一度日付毎に集計
  # @move_total = MovingTime.where("holiday_type = '休日'").group("DATE(dept_date)").sum(:moving_time).to_a
  
  # if @move_total.values{1} < 240
  #   @move_allowance = 2000
  # elseif @move_total.values < 360
  #   @move_allowance = 4000
  # else
  #   @move_allowance = 6000
  # end
  
  logger.debug @move_total.inspect
    
  @reports = Report.all
  @order = Order.find(@report.order_id)
  @user = User.find(@order.user_id)
  # @user_n = Transfer.where(:user_id => 1).first
  # @user_g = @user_n.grade_id
  # @grade_list = GradeFixed.where(:grade_id => @user_g)
  # @grade_value = @grade_list.where(:chanege_date < "2019-01-21").last
  # # @grade_n = Grade_fixed.
  # logger.debug @grade_value.inspect
  
  end

  # GET /reports/new
  def new
    @report = Report.new
  end

  # GET /reports/1/edit
  def edit
  end

  # POST /reports
  # POST /reports.json
  def create
    @report = Report.new(report_params)

    respond_to do |format|
      if @report.save
        format.html { redirect_to @report, notice: 'Report was successfully created.' }
        format.json { render :show, status: :created, location: @report }
      else
        format.html { render :new }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reports/1
  # PATCH/PUT /reports/1.json
  def update
    respond_to do |format|
      if @report.update(report_params)
        format.html { redirect_to @report, notice: 'Report was successfully updated.' }
        format.json { render :show, status: :ok, location: @report }
      else
        format.html { render :edit }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reports/1
  # DELETE /reports/1.json
  def destroy
    @report.destroy
    respond_to do |format|
      format.html { redirect_to reports_url, notice: 'Report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_report
      @report = Report.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def report_params
      params.require(:report).permit(:order_id, :purpose, :bussiness_trip, :address, :distance, :apply_date, :create_date, :adjusted_amount, :tmp_money, :return_money, :add_money)
    end
end
