class OthersController < ApplicationController
  before_action :set_other, only: [:show, :edit, :update, :destroy]

  # GET /others
  # GET /others.json
  def index
    @report = Report.where(:id => params[:report_id]).first
    @others = @report.others.all
    # @others = Other.all
  end

  # GET /others/1
  # GET /others/1.json
  def show
  end

  # GET /others/new
  def new
    @report = Report.where(:id => params[:report_id]).first
    @other = @report.others.build
  end

  # GET /others/1/edit
  def edit
  end

  # POST /others
  # POST /others.json
  def create
    # @other = Other.new(other_params)
    @report = Report.where(:id => params[:report_id]).first
    @other = @report.others.build(other_params)

    respond_to do |format|
      if @other.save
        # format.html { redirect_to @other, notice: 'Other was successfully created.' }
        format.html { redirect_to [@report,@other], notice: 'Other was successfully created.' }
        format.json { render :show, status: :created, location: @other }
      else
        format.html { render :new }
        format.json { render json: @other.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /others/1
  # PATCH/PUT /others/1.json
  def update
    respond_to do |format|
      if @other.update(other_params)
        # format.html { redirect_to @other, notice: 'Other was successfully updated.' }
        format.html { redirect_to [@report,@other], notice: 'Other was successfully updated.' }
        format.json { render :show, status: :ok, location: @other }
      else
        format.html { render :edit }
        format.json { render json: @other.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /others/1
  # DELETE /others/1.json
  def destroy
    @other.destroy
    respond_to do |format|
      format.html { redirect_to report_url(@other.report_id), notice: 'Other was successfully destroyed.' }
      # format.html { redirect_to report_others_url, notice: 'Other was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_other
      # @other = Other.find(params[:id])
      @report = Report.where(:id => params[:report_id]).first
      @other = @report.others.where(:id => params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def other_params
      params.require(:other).permit(:report_id, :payment_date, :content, :other_money)
    end
end
