class PathsController < ApplicationController
  before_action :set_path, only: [:show, :edit, :update, :destroy]

  # GET /paths
  # GET /paths.json
  def index
    # @paths = Path.all
    @report = Report.where(:id => params[:report_id]).first
    @moving_time = @report.moving_times.where(:id => params[:moving_time_id]).first
    @path = @moving_time.paths.all
  end

  # GET /paths/1
  # GET /paths/1.json
  def show
  end

  # GET /paths/new
  def new
    # @path = Path.new
    @report = Report.where(:id => params[:report_id]).first
    @moving_time = @report.moving_times.where(:id => params[:moving_time_id]).first
    @path = @moving_time.paths.build
  end

  # GET /paths/1/edit
  def edit
  end

  # POST /paths
  # POST /paths.json
  def create
    # @path = Path.new(path_params)
    @report = Report.where(:id => params[:report_id]).first
    # @moving_time = @report.moving_times.where(:id => params[:moving_time_id]).first
    @moving_time = MovingTime.where(:id => params[:moving_time_id]).first
    @path = @moving_time.paths.build(path_params)

    respond_to do |format|
      if @path.save
        # format.html { redirect_to @path, notice: 'Path was successfully created.' }
        format.html { redirect_to [@report,@moving_time,@path], notice: 'Daily allowance was successfully created.' }
        format.json { render :show, status: :created, location: @path }
      else
        format.html { render :new }
        format.json { render json: @path.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /paths/1
  # PATCH/PUT /paths/1.json
  def update
    respond_to do |format|
      if @path.update(path_params)
        # format.html { redirect_to @path, notice: 'Path was successfully updated.' }
        format.html { redirect_to [@report, @moving_time, @path], notice: 'Daily allowance was successfully updated.' }
        format.json { render :show, status: :ok, location: @path }
      else
        format.html { render :edit }
        format.json { render json: @path.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /paths/1
  # DELETE /paths/1.json
  def destroy
    @path.destroy
    respond_to do |format|
      # format.html { redirect_to paths_url, notice: 'Path was successfully destroyed.' }
      format.html { redirect_to report_url(@path.moving_time_id), notice: 'Daily allowance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_path
      # @path = Path.find(params[:id])
      @report = Report.where(:id => params[:report_id]).first
      # @moving_time = @report.moving_times.where(:id => params[:moving_time_id]).first
      @moving_time = MovingTime.where(:id => params[:moving_time_id]).first
      # @path = @moving_time.paths.where(:id => params[:id]).first
      @path = Path.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def path_params
      params.require(:path).permit(:moving_time_id, :dest_time, :dept_time, :point, :travel_hour)
    end
end
