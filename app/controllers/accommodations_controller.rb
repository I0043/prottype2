class AccommodationsController < ApplicationController
  before_action :set_accommodation, only: [:show, :edit, :update, :destroy]

  # GET /accommodations
  # GET /accommodations.json
  def index
    # @accommodations = Accommodation.all
    @report = Report.where(:id => params[:report_id]).first
    @accommodations = @report.accommodations.all
  end

  # GET /accommodations/1
  # GET /accommodations/1.json
  def show
  end

  # GET /accommodations/new
  def new
    # @accommodation = Accommodation.new
    @report = Report.where(:id => params[:report_id]).first
    @accommodation = @report.accommodations.build
    @city_name = CityName.all
    @city_name_lists = @city_name.pluck(:name)
    @city_name_lists_j = @city_name_lists.to_json.html_safe
    if @accommodation.add_money.blank?
      @accommodation.add_money = 0
    end
    @order = Order.find(@report.order_id)
    @user = User.find(@order.user_id)
    @grade = Gradelist.all
    @grade_select = @grade.find_by(grade: @user.grade)
    @grade_list_a = @grade_select.accommodation
    @grade_list_v = @grade_select.vehicle_night
  end

  # GET /accommodations/1/edit
  def edit
    @report = Report.where(:id => params[:report_id]).first
    @city_name = CityName.all
    @city_name_lists = @city_name.pluck(:name)
    @city_name_lists_j = @city_name_lists.to_json.html_safe
    if @accommodation.add_money.blank?
      @accommodation.add_money = 0
    end
    @order = Order.find(@report.order_id)
    @user = User.find(@order.user_id)
    @grade = Gradelist.all
    @grade_select = @grade.find_by(grade: @user.grade)
    @grade_list_a = @grade_select.accommodation
    @grade_list_v = @grade_select.vehicle_night
    logger.debug @grade_select.inspect
  end

  # POST /accommodations
  # POST /accommodations.json
  def create
    # @accommodation = Accommodation.new(accommodation_params)
    @report = Report.where(:id => params[:report_id]).first
    @accommodation = @report.accommodations.build(accommodation_params)
  
    
    # if @accommodation.city_name == "京都市" || @accommodation.city_name == "名古屋市" || @accommodation.city_name == "札幌市" || @accommodation.city_name == "横浜市" || @accommodation.city_name == "大阪市" || @accommodation.city_name == "神戸市" || @accommodation.city_name == "福岡市"
    #   @accommodation.add_money = 1000
    # else
    #   @accommodation.add_money = 0
    # end

    respond_to do |format|
      if @accommodation.save
        # format.html { redirect_to @accommodation, notice: 'Accommodation was successfully created.' }
        format.html { redirect_to [@report,@accommodation], notice: 'Accommodation was successfully created.' }
        format.json { render :show, status: :created, location: @accommodation }
      else
        format.html { render :new }
        format.json { render json: @accommodation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accommodations/1
  # PATCH/PUT /accommodations/1.json
  def update
    respond_to do |format|
      if @accommodation.update(accommodation_params)
        # format.html { redirect_to @accommodation, notice: 'Accommodation was successfully updated.' }
        format.html { redirect_to [@report,@accommodation], notice: 'Accommodation was successfully updated.' }
        format.json { render :show, status: :ok, location: @accommodation }
      else
        format.html { render :edit }
        format.json { render json: @accommodation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accommodations/1
  # DELETE /accommodations/1.json
  def destroy
    @accommodation.destroy
    respond_to do |format|
      # format.html { redirect_to accommodations_url, notice: 'Accommodation was successfully destroyed.' }
      format.html { redirect_to report_url(@accommodation.report_id), notice: 'Accommodation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_accommodation
      # @accommodation = Accommodation.find(params[:id])
      @report = Report.where(:id => params[:report_id]).first
      @accommodation = @report.accommodations.where(:id => params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def accommodation_params
      params.require(:accommodation).permit(:report_id, :checkin, :checkout, :city_name, :accommodation, :add_money)
    end
end
