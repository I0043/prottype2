class MovingRoutesController < ApplicationController
  before_action :set_moving_route, only: [:show, :edit, :update, :destroy]

  # GET /moving_routes
  # GET /moving_routes.json
  def index
    # @moving_routes = MovingRoute.all
    @report = Report.where(:id => params[:report_id]).first
    @moving_time = @report.moving_times.where(:id => params[:moving_time_id]).first
    @moving_routes = @moving_time.moving_routes.all
  end

  # GET /moving_routes/1
  # GET /moving_routes/1.json
  def show
  end

  # GET /moving_routes/new
  def new
    # @moving_route = MovingRoute.new
    @report = Report.where(:id => params[:report_id]).first
    @moving_time = @report.moving_times.where(:id => params[:moving_time_id]).first
    @moving_route = @moving_time.moving_routes.build
  end

  # GET /moving_routes/1/edit
  def edit
  end

  # POST /moving_routes
  # POST /moving_routes.json
  def create
    # @moving_route = MovingRoute.new(moving_route_params)
    @report = Report.where(:id => params[:report_id]).first
    # @moving_time = @report.moving_times.where(:id => params[:moving_time_id]).first
    @moving_time = MovingTime.where(:id => params[:moving_time_id]).first
    @moving_route = @moving_time.moving_routes.build(moving_route_params)


    respond_to do |format|
      if @moving_route.save
        # format.html { redirect_to @moving_route, notice: 'Moving route was successfully created.' }
        format.html { redirect_to [@report,@moving_time,@moving_route], notice: 'Daily allowance was successfully created.' }
        format.json { render :show, status: :created, location: @moving_route }
      else
        format.html { render :new }
        format.json { render json: @moving_route.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /moving_routes/1
  # PATCH/PUT /moving_routes/1.json
  def update
    respond_to do |format|
      if @moving_route.update(moving_route_params)
        # format.html { redirect_to @moving_route, notice: 'Moving route was successfully updated.' }
        format.html { redirect_to [@report, @moving_time, @moving_route], notice: 'Daily allowance was successfully updated.' }
        format.json { render :show, status: :ok, location: @moving_route }
      else
        format.html { render :edit }
        format.json { render json: @moving_route.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /moving_routes/1
  # DELETE /moving_routes/1.json
  def destroy
    @moving_route.destroy
    respond_to do |format|
      # format.html { redirect_to moving_routes_url, notice: 'Moving route was successfully destroyed.' }
      format.html { redirect_to report_url(@moving_route.moving_time_id), notice: 'Daily allowance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_moving_route
      # @moving_route = MovingRoute.find(params[:id])
      @report = Report.where(:id => params[:report_id]).first
      # @moving_time = @report.moving_times.where(:id => params[:moving_time_id]).first
      @moving_time = MovingTime.where(:id => params[:moving_time_id]).first
      # @path = @moving_time.paths.where(:id => params[:id]).first
      @moving_route = MovingRoute.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def moving_route_params
      params.require(:moving_route).permit(:moving_time_id, :dest_time, :dept_time, :point, :travel_hour, :dest, :access, :exclusion_type, :drive_type, :transport_money)
    end
end
