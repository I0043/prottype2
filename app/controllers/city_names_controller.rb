class CityNamesController < ApplicationController
  before_action :set_city_name, only: [:show, :edit, :update, :destroy]

  # GET /city_names
  # GET /city_names.json
  def index
    @city_names = CityName.all
  end

  # GET /city_names/1
  # GET /city_names/1.json
  def show
  end

  # GET /city_names/new
  def new
    @city_name = CityName.new
  end

  # GET /city_names/1/edit
  def edit
  end

  # POST /city_names
  # POST /city_names.json
  def create
    @city_name = CityName.new(city_name_params)

    respond_to do |format|
      if @city_name.save
        format.html { redirect_to @city_name, notice: 'City name was successfully created.' }
        format.json { render :show, status: :created, location: @city_name }
      else
        format.html { render :new }
        format.json { render json: @city_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /city_names/1
  # PATCH/PUT /city_names/1.json
  def update
    respond_to do |format|
      if @city_name.update(city_name_params)
        format.html { redirect_to @city_name, notice: 'City name was successfully updated.' }
        format.json { render :show, status: :ok, location: @city_name }
      else
        format.html { render :edit }
        format.json { render json: @city_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /city_names/1
  # DELETE /city_names/1.json
  def destroy
    @city_name.destroy
    respond_to do |format|
      format.html { redirect_to city_names_url, notice: 'City name was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_city_name
      @city_name = CityName.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def city_name_params
      params.require(:city_name).permit(:name)
    end
end
