class MovingTimesController < ApplicationController
  before_action :set_moving_time, only: [:show, :edit, :update, :destroy]

  # GET /moving_times
  # GET /moving_times.json
  def index
    # @moving_times = MovingTime.all
    @report = Report.where(:id => params[:report_id]).first
    @moving_times = @report.moving_times.all
    @moving_id = @report.moving_times.where(:id => params[:moving_time_id]).first
  end

  # GET /moving_times/1
  # GET /moving_times/1.json
  def show
  end

  # GET /moving_times/new
  def new
    # @moving_time = MovingTime.new
    @report = Report.where(:id => params[:report_id]).first
    @moving_time = @report.moving_times.build
  end

  # GET /moving_times/1/edit
  def edit
  end

  # POST /moving_times
  # POST /moving_times.json
  def create
    # @moving_time = MovingTime.new(moving_time_params)
    @report = Report.where(:id => params[:report_id]).first
    @moving_time = @report.moving_times.build(moving_time_params)

    respond_to do |format|
      if @moving_time.save
        # format.html { redirect_to @moving_time, notice: 'Moving time was successfully created.' }
        format.html { redirect_to [@report,@moving_time], notice: 'Daily allowance was successfully created.' }
        format.json { render :show, status: :created, location: @moving_time }
      else
        format.html { render :new }
        format.json { render json: @moving_time.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /moving_times/1
  # PATCH/PUT /moving_times/1.json
  def update
    respond_to do |format|
      if @moving_time.update(moving_time_params)
        # format.html { redirect_to @moving_time, notice: 'Moving time was successfully updated.' }
        format.html { redirect_to [@report,@moving_time], notice: 'Daily allowance was successfully updated.' }
        format.json { render :show, status: :ok, location: @moving_time }
      else
        format.html { render :edit }
        format.json { render json: @moving_time.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /moving_times/1
  # DELETE /moving_times/1.json
  def destroy
    @moving_time.destroy
    respond_to do |format|
      # format.html { redirect_to moving_times_url, notice: 'Moving time was successfully destroyed.' }
      format.html { redirect_to report_url(@moving_time.report_id), notice: 'Daily allowance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_moving_time
      # @moving_time = MovingTime.find(params[:id])
      @report = Report.where(:id => params[:report_id]).first
      @moving_time = @report.moving_times.where(:id => params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def moving_time_params
      params.require(:moving_time).permit(:dept_date, :dest_date, :dest_price, :dept_place, :moving_time, :allowance, :route_type, :holiday_type, :memo, :course )
    end
end
