class DailyAllowancesController < ApplicationController
  before_action :set_daily_allowance, only: [:show, :edit, :update, :destroy]

  # GET /daily_allowances
  # GET /daily_allowances.json
  def index
    # @daily_allowances = DailyAllowance.all
    @report = Report.where(:id => params[:report_id]).first
    @daily_allowances = @report.daily_allowances.all
  end

  # GET /daily_allowances/1
  # GET /daily_allowances/1.json
  def show
  end

  # GET /daily_allowances/new
  def new
    # @daily_allowance = DailyAllowance.new
    @report = Report.where(:id => params[:report_id]).first
    @daily_allowance = @report.daily_allowances.build
    @grade = Gradelist.all
    @grade_list = @grade.pluck(:daily_allowance)
    @holiday = Calendar.where("holiday = '1'")
    
    @test = @holiday.exists?(:work_date => "2018-10-07")
    logger.debug @grade_list.inspect
  end

  # GET /daily_allowances/1/edit
  def edit
    @order = Order.find(@report.order_id)
    @user = User.find(@order.user_id)
    @grade = Gradelist.all
    @grade_select = @grade.find_by(grade: @user.grade)
    @grade_list_d = @grade_select.daily_allowance
    @calendar = Calendar.where(:work_date => '#daily_allowance_trip_date').first
    logger.debug @calendar.inspect
  end

  # POST /daily_allowances
  # POST /daily_allowances.json
  def create
    # @daily_allowance = DailyAllowance.new(daily_allowance_params)
    @report = Report.where(:id => params[:report_id]).first
    @daily_allowance = @report.daily_allowances.build(daily_allowance_params)

    respond_to do |format|
      if @daily_allowance.save
        # format.html { redirect_to @daily_allowance, notice: 'Daily allowance was successfully created.' }
        format.html { redirect_to [@report,@daily_allowance], notice: 'Daily allowance was successfully created.' }
        format.json { render :show, status: :created, location: @daily_allowance }
      else
        format.html { render :new }
        format.json { render json: @daily_allowance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /daily_allowances/1
  # PATCH/PUT /daily_allowances/1.json
  def update
    respond_to do |format|
      if @daily_allowance.update(daily_allowance_params)
        # format.html { redirect_to @daily_allowance, notice: 'Daily allowance was successfully updated.' }
        format.html { redirect_to [@report,@daily_allowance], notice: 'Daily allowance was successfully updated.' }
        format.json { render :show, status: :ok, location: @daily_allowance }
      else
        format.html { render :edit }
        format.json { render json: @daily_allowance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /daily_allowances/1
  # DELETE /daily_allowances/1.json
  def destroy
    @daily_allowance.destroy
    respond_to do |format|
      # format.html { redirect_to daily_allowances_url, notice: 'Daily allowance was successfully destroyed.' }
      format.html { redirect_to report_url(@daily_allowance.report_id), notice: 'Daily allowance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_daily_allowance
      # @daily_allowance = DailyAllowance.find(params[:id])
      @report = Report.where(:id => params[:report_id]).first
      @daily_allowance = @report.daily_allowances.where(:id => params[:id]).first
      # @daily_allowance = DailyAllowance.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def daily_allowance_params
      params.require(:daily_allowance).permit(:report_id, :trip_date, :holiday, :fixed, :fixed_over, :moving_allowance, :moving_type, :start_time, :finish_time)
    end
end
