class EtcListsController < ApplicationController
  before_action :set_etc_list, only: [:show, :edit, :update, :destroy]

  # GET /etc_lists
  # GET /etc_lists.json
  def index
    @etc_lists = EtcList.all
  end

  # GET /etc_lists/1
  # GET /etc_lists/1.json
  def show
  end

  # GET /etc_lists/new
  def new
    @etc_list = EtcList.new
  end

  # GET /etc_lists/1/edit
  def edit
  end

  # POST /etc_lists
  # POST /etc_lists.json
  def create
    @etc_list = EtcList.new(etc_list_params)

    respond_to do |format|
      if @etc_list.save
        format.html { redirect_to @etc_list, notice: 'Etc list was successfully created.' }
        format.json { render :show, status: :created, location: @etc_list }
      else
        format.html { render :new }
        format.json { render json: @etc_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /etc_lists/1
  # PATCH/PUT /etc_lists/1.json
  def update
    respond_to do |format|
      if @etc_list.update(etc_list_params)
        format.html { redirect_to @etc_list, notice: 'Etc list was successfully updated.' }
        format.json { render :show, status: :ok, location: @etc_list }
      else
        format.html { render :edit }
        format.json { render json: @etc_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /etc_lists/1
  # DELETE /etc_lists/1.json
  def destroy
    @etc_list.destroy
    respond_to do |format|
      format.html { redirect_to etc_lists_url, notice: 'Etc list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_etc_list
      @etc_list = EtcList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def etc_list_params
      params.require(:etc_list).permit(:name)
    end
end
