class DailyCostsController < ApplicationController
  before_action :set_daily_cost, only: [:show, :edit, :update, :destroy]
  before_action :daily_cost_value, only: [:new, :edit]
  
  # GET /daily_costs
  # GET /daily_costs.json
  def index
    # @daily_costs = DailyCost.all
    @report = Report.where(:id => params[:report_id]).first
    @daily_costs = @report.daily_costs.all
  end

  # GET /daily_costs/1
  # GET /daily_costs/1.json
  def show
  end

  # GET /daily_costs/new
  def new
    # @daily_cost = DailyCost.new
    @report = Report.where(:id => params[:report_id]).first
    @daily_cost = @report.daily_costs.build
  end

  # GET /daily_costs/1/edit
  def edit
  end

  # POST /daily_costs
  # POST /daily_costs.json
  def create
    @daily_cost = DailyCost.new(daily_cost_params)
    @report = Report.where(:id => params[:report_id]).first
    @daily_cost = @report.daily_costs.build(daily_cost_params)

    respond_to do |format|
      if @daily_cost.save
        # format.html { redirect_to @daily_cost, notice: 'Daily cost was successfully created.' }
        format.html { redirect_to [@report,@daily_cost], notice: 'Daily allowance was successfully created.' }
        format.json { render :show, status: :created, location: @daily_cost }
      else
        format.html { render :new }
        format.json { render json: @daily_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /daily_costs/1
  # PATCH/PUT /daily_costs/1.json
  def update
    respond_to do |format|
      if @daily_cost.update(daily_cost_params)
        # format.html { redirect_to @daily_cost, notice: 'Daily cost was successfully updated.' }
        format.html { redirect_to [@report,@daily_cost], notice: 'Daily allowance was successfully updated.' }
        format.json { render :show, status: :ok, location: @daily_cost }
        format.js { @status = "success"}
      else
        format.html { render :edit }
        format.json { render json: @daily_cost.errors, status: :unprocessable_entity }
        format.js { @status = "fail" }
      end
    end
  end

  # DELETE /daily_costs/1
  # DELETE /daily_costs/1.json
  def destroy
    @daily_cost.destroy
    respond_to do |format|
      # format.html { redirect_to daily_costs_url, notice: 'Daily cost was successfully destroyed.' }
      format.html { redirect_to report_url(@daily_cost.report_id), notice: 'Daily allowance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_daily_cost
      # @daily_cost = DailyCost.find(params[:id])
      @report = Report.where(:id => params[:report_id]).first
      @daily_cost = @report.daily_costs.where(:id => params[:id]).first
    end
    
    def daily_cost_value
      @report = Report.where(:id => params[:report_id]).first
      @city_name_lists = CityName.pluck(:name)
      @order = Order.find(@report.order_id)
      @user = User.find(@order.user_id)
      @grade_select = Gradelist.find_by(grade: @user.grade)
      @holiday = Calendar.where("holiday = '1'")
      @holiday_j = @holiday.pluck(:work_date).to_json.html_safe
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def daily_cost_params
      params.require(:daily_cost).permit(:report_id, :daily_date, :expenses_type, :work_type, :dept_time, :dest_ime, :city_name, :course, :travel_hour, :daily_cost)
    end
end
