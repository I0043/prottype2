class FixedsController < ApplicationController
  before_action :set_fixed, only: [:show, :edit, :update, :destroy]

  # GET /fixeds
  # GET /fixeds.json
  def index
    @fixeds = Fixed.all
  end

  # GET /fixeds/1
  # GET /fixeds/1.json
  def show
  end

  # GET /fixeds/new
  def new
    @fixed = Fixed.new
  end

  # GET /fixeds/1/edit
  def edit
  end

  # POST /fixeds
  # POST /fixeds.json
  def create
    @fixed = Fixed.new(fixed_params)

    respond_to do |format|
      if @fixed.save
        format.html { redirect_to @fixed, notice: 'Fixed was successfully created.' }
        format.json { render :show, status: :created, location: @fixed }
      else
        format.html { render :new }
        format.json { render json: @fixed.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fixeds/1
  # PATCH/PUT /fixeds/1.json
  def update
    respond_to do |format|
      if @fixed.update(fixed_params)
        format.html { redirect_to @fixed, notice: 'Fixed was successfully updated.' }
        format.json { render :show, status: :ok, location: @fixed }
      else
        format.html { render :edit }
        format.json { render json: @fixed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fixeds/1
  # DELETE /fixeds/1.json
  def destroy
    @fixed.destroy
    respond_to do |format|
      format.html { redirect_to fixeds_url, notice: 'Fixed was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fixed
      @fixed = Fixed.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fixed_params
      params.require(:fixed).permit(:fixed_type, :fixed)
    end
end
