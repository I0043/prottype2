class GradeFixedsController < ApplicationController
  before_action :set_grade_fixed, only: [:show, :edit, :update, :destroy]

  # GET /grade_fixeds
  # GET /grade_fixeds.json
  def index
    @grade_fixeds = GradeFixed.all
  end

  # GET /grade_fixeds/1
  # GET /grade_fixeds/1.json
  def show
  end

  # GET /grade_fixeds/new
  def new
    @grade_fixed = GradeFixed.new
  end

  # GET /grade_fixeds/1/edit
  def edit
  end

  # POST /grade_fixeds
  # POST /grade_fixeds.json
  def create
    @grade_fixed = GradeFixed.new(grade_fixed_params)

    respond_to do |format|
      if @grade_fixed.save
        format.html { redirect_to @grade_fixed, notice: 'Grade fixed was successfully created.' }
        format.json { render :show, status: :created, location: @grade_fixed }
      else
        format.html { render :new }
        format.json { render json: @grade_fixed.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /grade_fixeds/1
  # PATCH/PUT /grade_fixeds/1.json
  def update
    respond_to do |format|
      if @grade_fixed.update(grade_fixed_params)
        format.html { redirect_to @grade_fixed, notice: 'Grade fixed was successfully updated.' }
        format.json { render :show, status: :ok, location: @grade_fixed }
      else
        format.html { render :edit }
        format.json { render json: @grade_fixed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /grade_fixeds/1
  # DELETE /grade_fixeds/1.json
  def destroy
    @grade_fixed.destroy
    respond_to do |format|
      format.html { redirect_to grade_fixeds_url, notice: 'Grade fixed was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_grade_fixed
      @grade_fixed = GradeFixed.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def grade_fixed_params
      params.require(:grade_fixed).permit(:fixed_id, :grade_id, :change_date)
    end
end
