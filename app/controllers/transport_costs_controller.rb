class TransportCostsController < ApplicationController
  before_action :set_transport_cost, only: [:show, :edit, :update, :destroy]

  # GET /transport_costs
  # GET /transport_costs.json
  def index
    # @transport_costs = TransportCost.all
    @report = Report.where(:id => params[:report_id]).first
    @transport_costs = @report.transport_costs.all
  end

  # GET /transport_costs/1
  # GET /transport_costs/1.json
  def show
  end

  # GET /transport_costs/new
  def new
    # @transport_cost = TransportCost.new
    @report = Report.where(:id => params[:report_id]).first
    @transport_cost = @report.transport_costs.build
    @access_type_lists = AccessType.all.pluck(:moving_type)
    @etc_list = EtcList.all.pluck(:name)
  end

  # GET /transport_costs/1/edit
  def edit
    @access_type_lists = AccessType.all.pluck(:moving_type)
    @etc_list = EtcList.all.pluck(:name)
  end

  # POST /transport_costs
  # POST /transport_costs.json
  def create
    # @transport_cost = TransportCost.new(transport_cost_params)
    @report = Report.where(:id => params[:report_id]).first
    @transport_cost = @report.transport_costs.build(transport_cost_params)

    respond_to do |format|
      if @transport_cost.save
        # format.html { redirect_to @transport_cost, notice: 'Transport cost was successfully created.' }
        format.html { redirect_to [@report,@transport_cost], notice: 'Transport cost was successfully created.' }
        format.json { render :show, status: :created, location: @transport_cost }
      else
        format.html { render :new }
        format.json { render json: @transport_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transport_costs/1
  # PATCH/PUT /transport_costs/1.json
  def update
    respond_to do |format|
      if @transport_cost.update(transport_cost_params)
        # format.html { redirect_to @transport_cost, notice: 'Transport cost was successfully updated.' }
        format.html { redirect_to [@report,@transport_cost], notice: 'Transport cost was successfully updated.' }
        format.json { render :show, status: :ok, location: @transport_cost }
      else
        format.html { render :edit }
        format.json { render json: @transport_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transport_costs/1
  # DELETE /transport_costs/1.json
  def destroy
    @transport_cost.destroy
    respond_to do |format|
      # format.html { redirect_to transport_costs_url, notice: 'Transport cost was successfully destroyed.' }
      format.html { redirect_to report_url(@transport_cost.report_id), notice: 'Transport cost was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transport_cost
      # @transport_cost = TransportCost.find(params[:id])
      @report = Report.where(:id => params[:report_id]).first
      @transport_cost = @report.transport_costs.where(:id => params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transport_cost_params
      params.require(:transport_cost).permit(:report_id, :move_date, :dept, :dest, :root, :access, :etc, :drive_type, :transport_cost)
    end
end
