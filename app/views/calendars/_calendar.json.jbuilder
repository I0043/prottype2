json.extract! calendar, :id, :work_date, :holiday, :created_at, :updated_at
json.url calendar_url(calendar, format: :json)
