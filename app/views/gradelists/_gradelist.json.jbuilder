json.extract! gradelist, :id, :grade, :fixed, :created_at, :updated_at
json.url gradelist_url(gradelist, format: :json)
