json.extract! path, :id, :moving_time_id, :dest_time, :dept_time, :point, :moving_time, :created_at, :updated_at
json.url path_url(path, format: :json)
