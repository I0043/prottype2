json.extract! grade_fixed, :id, :fixed_id, :grade_id, :change_date, :created_at, :updated_at
json.url grade_fixed_url(grade_fixed, format: :json)
