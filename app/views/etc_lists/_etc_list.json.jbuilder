json.extract! etc_list, :id, :name, :created_at, :updated_at
json.url etc_list_url(etc_list, format: :json)
