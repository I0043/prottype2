json.extract! daily_allowance, :id, :report_id, :trip_date, :holiday, :fixed, :fixed_over, :moving_allowance, :created_at, :updated_at
json.url daily_allowance_url(daily_allowance, format: :json)
