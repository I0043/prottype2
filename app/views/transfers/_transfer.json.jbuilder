json.extract! transfer, :id, :user_id, :grade_id, :transfer_type, :transfer_date, :created_at, :updated_at
json.url transfer_url(transfer, format: :json)
