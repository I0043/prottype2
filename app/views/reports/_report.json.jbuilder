json.extract! report, :id, :apply_date, :create_date, :adjusted_amount, :tmp_money, :return_money, :add_money, :created_at, :updated_at
json.url report_url(report, format: :json)
