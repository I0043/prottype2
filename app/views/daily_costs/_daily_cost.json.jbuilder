json.extract! daily_cost, :id, :report_id, :daily_date, :expenses_type, :work_type, :dept_time, :dest_ime, :city_name, :course, :travel_hour, :daily_cost, :created_at, :updated_at
json.url daily_cost_url(daily_cost, format: :json)
