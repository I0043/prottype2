json.extract! access_type, :id, :moving_type, :created_at, :updated_at
json.url access_type_url(access_type, format: :json)
