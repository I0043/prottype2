json.extract! order, :id, :user_id, :udno, :skno, :product_name, :customer, :created_at, :updated_at
json.url order_url(order, format: :json)
