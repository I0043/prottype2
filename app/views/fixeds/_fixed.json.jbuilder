json.extract! fixed, :id, :fixed_type, :fixed, :created_at, :updated_at
json.url fixed_url(fixed, format: :json)
