json.extract! accommodation, :id, :report_id, :checkin, :checkout, :city_name, :accommodation, :add_money, :created_at, :updated_at
json.url accommodation_url(accommodation, format: :json)
