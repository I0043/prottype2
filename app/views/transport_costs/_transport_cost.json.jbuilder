json.extract! transport_cost, :id, :report_id, :move_date, :dept, :dest, :root, :access, :ETC, :drive_type, :transport_cost, :created_at, :updated_at
json.url transport_cost_url(transport_cost, format: :json)
