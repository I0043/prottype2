json.extract! additional, :id, :report_id, :additional_date, :moving_type, :dest_time, :dept_time, :additional, :vehicle_night, :created_at, :updated_at
json.url additional_url(additional, format: :json)
