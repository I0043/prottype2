json.extract! moving_route, :id, :moving_time_id, :dest_time, :dept_time, :point, :travel_hour, :created_at, :updated_at
json.url moving_route_url(moving_route, format: :json)
