json.extract! moving_time, :id, :dept_date, :dest_date, :place, :moving_time, :created_at, :updated_at
json.url moving_time_url(moving_time, format: :json)
