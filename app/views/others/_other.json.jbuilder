json.extract! other, :id, :report_id, :payment_date, :content, :other_money, :created_at, :updated_at
json.url other_url(other, format: :json)
