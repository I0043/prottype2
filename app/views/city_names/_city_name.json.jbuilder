json.extract! city_name, :id, :name, :created_at, :updated_at
json.url city_name_url(city_name, format: :json)
