# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190121083045) do

  create_table "access_types", force: :cascade do |t|
    t.string "moving_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "accommodations", force: :cascade do |t|
    t.integer "report_id"
    t.date "checkin"
    t.date "checkout"
    t.string "city_name"
    t.integer "accommodation"
    t.integer "add_money"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["report_id"], name: "index_accommodations_on_report_id"
  end

  create_table "additionals", force: :cascade do |t|
    t.integer "report_id"
    t.date "additional_date"
    t.string "moving_type"
    t.time "dest_time"
    t.time "dept_time"
    t.integer "additional"
    t.integer "vehicle_night"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["report_id"], name: "index_additionals_on_report_id"
  end

  create_table "calendars", force: :cascade do |t|
    t.date "work_date"
    t.integer "holiday"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "city_names", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "daily_allowances", force: :cascade do |t|
    t.integer "report_id"
    t.date "trip_date"
    t.string "holiday"
    t.integer "fixed"
    t.integer "fixed_over"
    t.integer "moving_allowance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "moving_type"
    t.time "start_time"
    t.time "finish_time"
    t.index ["report_id"], name: "index_daily_allowances_on_report_id"
  end

  create_table "daily_costs", force: :cascade do |t|
    t.integer "report_id"
    t.date "daily_date"
    t.string "expenses_type"
    t.string "work_type"
    t.time "dept_time"
    t.time "dest_ime"
    t.string "city_name"
    t.string "course"
    t.integer "travel_hour"
    t.integer "daily_cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["report_id"], name: "index_daily_costs_on_report_id"
  end

  create_table "etc_lists", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fixeds", force: :cascade do |t|
    t.string "fixed_type"
    t.integer "fixed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "grade_fixeds", force: :cascade do |t|
    t.integer "fixed_id"
    t.integer "grade_id"
    t.date "change_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["fixed_id"], name: "index_grade_fixeds_on_fixed_id"
    t.index ["grade_id"], name: "index_grade_fixeds_on_grade_id"
  end

  create_table "gradelists", force: :cascade do |t|
    t.integer "grade"
    t.integer "fixed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "accommodation"
    t.integer "daily_allowance"
    t.integer "vehicle_night"
  end

  create_table "grades", force: :cascade do |t|
    t.integer "grade"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "move_routes", force: :cascade do |t|
    t.date "dept_date"
    t.date "dest_date"
    t.string "place"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "moving_paths", force: :cascade do |t|
    t.integer "moving_time_id"
    t.datetime "dest_time"
    t.datetime "dept_time"
    t.string "point"
    t.integer "moving_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["moving_time_id"], name: "index_moving_paths_on_moving_time_id"
  end

  create_table "moving_routes", force: :cascade do |t|
    t.integer "moving_time_id"
    t.datetime "dest_time"
    t.datetime "dept_time"
    t.string "point"
    t.integer "travel_hour"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "dest"
    t.string "access"
    t.string "exclusion_type"
    t.string "drive_type"
    t.integer "transport_money"
    t.index ["moving_time_id"], name: "index_moving_routes_on_moving_time_id"
  end

  create_table "moving_times", force: :cascade do |t|
    t.datetime "dept_date"
    t.datetime "dest_date"
    t.string "dept_place"
    t.integer "moving_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "dest_price"
    t.integer "report_id"
    t.integer "allowance"
    t.string "route_type"
    t.string "holiday_type"
    t.string "memo"
    t.string "course"
    t.index ["report_id"], name: "index_moving_times_on_report_id"
  end

  create_table "movings", force: :cascade do |t|
    t.time "dept_date"
    t.time "dest_date"
    t.string "place"
    t.integer "moving_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer "user_id"
    t.string "udno"
    t.string "skno"
    t.string "product_name"
    t.string "customer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "others", force: :cascade do |t|
    t.integer "report_id"
    t.date "payment_date"
    t.string "content"
    t.integer "other_money"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["report_id"], name: "index_others_on_report_id"
  end

  create_table "paths", force: :cascade do |t|
    t.integer "moving_time_id"
    t.datetime "dest_time"
    t.datetime "dept_time"
    t.string "point"
    t.integer "travel_hour"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["moving_time_id"], name: "index_paths_on_moving_time_id"
  end

  create_table "reports", force: :cascade do |t|
    t.date "apply_date"
    t.date "create_date"
    t.integer "adjusted_amount"
    t.integer "tmp_money"
    t.integer "return_money"
    t.integer "add_money"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "order_id"
    t.string "purpose"
    t.string "bussiness_trip"
    t.string "address"
    t.integer "distance"
    t.index ["order_id"], name: "index_reports_on_order_id"
  end

  create_table "transfers", force: :cascade do |t|
    t.integer "user_id"
    t.integer "grade_id"
    t.string "transfer_type"
    t.date "transfer_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["grade_id"], name: "index_transfers_on_grade_id"
    t.index ["user_id"], name: "index_transfers_on_user_id"
  end

  create_table "transport_costs", force: :cascade do |t|
    t.integer "report_id"
    t.date "move_date"
    t.string "dept"
    t.string "dest"
    t.string "root"
    t.string "access"
    t.string "etc"
    t.string "drive_type"
    t.integer "transport_cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["report_id"], name: "index_transport_costs_on_report_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "emp_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "grade"
  end

end
