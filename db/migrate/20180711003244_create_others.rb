class CreateOthers < ActiveRecord::Migration[5.1]
  def change
    create_table :others do |t|
      t.references :report, foreign_key: true
      t.date :payment_date
      t.string :content
      t.integer :other_money

      t.timestamps
    end
  end
end
