class CreateMovingRoutes < ActiveRecord::Migration[5.1]
  def change
    create_table :moving_routes do |t|
      t.references :moving_time, foreign_key: true
      t.datetime :dest_time
      t.datetime :dept_time
      t.string :point
      t.integer :travel_hour

      t.timestamps
    end
  end
end
