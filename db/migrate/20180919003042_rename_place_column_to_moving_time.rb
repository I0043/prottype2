class RenamePlaceColumnToMovingTime < ActiveRecord::Migration[5.1]
  def change
    rename_column :moving_times, :place, :dept_place
  end
end
