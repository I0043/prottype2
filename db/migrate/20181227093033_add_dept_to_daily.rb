class AddDeptToDaily < ActiveRecord::Migration[5.1]
  def up
    change_table :daily_costs do |t|
      t.change :dest_ime, :time
    end
  end

  def down
    change_table :daily_costs do |t|
      t.change :dest_time, :datetime
    end
  end
end
