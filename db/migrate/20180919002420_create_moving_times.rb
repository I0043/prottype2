class CreateMovingTimes < ActiveRecord::Migration[5.1]
  def change
    create_table :moving_times do |t|
      t.datetime :dept_date
      t.datetime :dest_date
      t.string :place
      t.integer :moving_time

      t.timestamps
    end
  end
end
