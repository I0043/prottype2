class AddColumnMovingtime2 < ActiveRecord::Migration[5.1]
  def change
    add_reference :moving_times, :report, foreign_key: true
  end
end
