class AddtransportToroutes < ActiveRecord::Migration[5.1]
  def change
    add_column :moving_routes, :dest, :string
    add_column :moving_routes, :access, :string
    add_column :moving_routes, :exclusion_type, :string
    add_column :moving_routes, :drive_type, :string
    add_column :moving_routes, :transport_money, :integer
  end
end
