class AddDestToDaily < ActiveRecord::Migration[5.1]
  def up
    change_table :daily_costs do |t|
      t.change :dept_time, :time
    end
  end

  def down
    change_table :daily_costs do |t|
      t.change :dept_time, :datetime
    end
  end
end
