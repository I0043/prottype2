class RenameTimesColumnToPaths < ActiveRecord::Migration[5.1]
  def change
    rename_column :paths, :moving_time, :travel_hour
  end
end
