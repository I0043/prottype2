class AddOrderRefToReports < ActiveRecord::Migration[5.1]
  def change
    add_reference :reports, :order, foreign_key: true
  end
end
