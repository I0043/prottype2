class CreateGradeFixeds < ActiveRecord::Migration[5.1]
  def change
    create_table :grade_fixeds do |t|
      t.references :fixed, foreign_key: true
      t.references :grade, foreign_key: true
      t.date :change_date

      t.timestamps
    end
  end
end
