class AddTitlesToReports < ActiveRecord::Migration[5.1]
  def change
    add_column :reports, :purpose, :string
    add_column :reports, :bussiness_trip, :string
    add_column :reports, :address, :string
    add_column :reports, :distance, :integer
  end
end
