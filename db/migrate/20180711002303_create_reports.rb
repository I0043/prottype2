class CreateReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
      t.date :apply_date
      t.date :create_date
      t.integer :adjusted_amount
      t.integer :tmp_money
      t.integer :return_money
      t.integer :add_money

      t.timestamps
    end
  end
end
