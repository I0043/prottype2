class CreateAccessTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :access_types do |t|
      t.string :moving_type

      t.timestamps
    end
  end
end
