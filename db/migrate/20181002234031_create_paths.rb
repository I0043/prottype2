class CreatePaths < ActiveRecord::Migration[5.1]
  def change
    create_table :paths do |t|
      t.references :moving_time, foreign_key: true
      t.datetime :dest_time
      t.datetime :dept_time
      t.string :point
      t.integer :moving_time

      t.timestamps
    end
  end
end
