class CreateAdditionals < ActiveRecord::Migration[5.1]
  def change
    create_table :additionals do |t|
      t.references :report, foreign_key: true
      t.date :additional_date
      t.string :moving_type
      t.time :dest_time
      t.time :dept_time
      t.integer :additional
      t.integer :vehicle_night

      t.timestamps
    end
  end
end
