class AddFixedToGradelists < ActiveRecord::Migration[5.1]
  def change
    add_column :gradelists, :accommodation, :integer
    add_column :gradelists, :daily_allowance, :integer
    add_column :gradelists, :vehicle_night, :integer
  end
end
