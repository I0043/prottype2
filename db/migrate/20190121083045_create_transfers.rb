class CreateTransfers < ActiveRecord::Migration[5.1]
  def change
    create_table :transfers do |t|
      t.references :user, foreign_key: true
      t.references :grade, foreign_key: true
      t.string :transfer_type
      t.date :transfer_date

      t.timestamps
    end
  end
end
