class CreateDailyCosts < ActiveRecord::Migration[5.1]
  def change
    create_table :daily_costs do |t|
      t.references :report, foreign_key: true
      t.date :daily_date
      t.string :expenses_type
      t.string :work_type
      t.datetime :dept_time
      t.datetime :dest_ime
      t.string :city_name
      t.string :course
      t.integer :travel_hour
      t.integer :daily_cost

      t.timestamps
    end
  end
end
