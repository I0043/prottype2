class AddCourseToMovingTimes < ActiveRecord::Migration[5.1]
  def change
    add_column :moving_times, :course, :string
  end
end
