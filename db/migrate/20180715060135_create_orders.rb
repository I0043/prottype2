class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.references :user, foreign_key: true
      t.string :udno
      t.string :skno
      t.string :product_name
      t.string :customer

      t.timestamps
    end
  end
end
