class CreateGradelists < ActiveRecord::Migration[5.1]
  def change
    create_table :gradelists do |t|
      t.integer :grade
      t.integer :fixed

      t.timestamps
    end
  end
end
