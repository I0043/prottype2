class AddColumnsToMovingtimes < ActiveRecord::Migration[5.1]
  def change
    add_column :moving_times, :route_type, :string
    add_column :moving_times, :holiday_type, :string
    add_column :moving_times, :memo, :string
  end
end
