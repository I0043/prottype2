class CreateFixeds < ActiveRecord::Migration[5.1]
  def change
    create_table :fixeds do |t|
      t.string :fixed_type
      t.integer :fixed

      t.timestamps
    end
  end
end
