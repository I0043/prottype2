class CreateDailyAllowances < ActiveRecord::Migration[5.1]
  def change
    create_table :daily_allowances do |t|
      t.references :report, foreign_key: true
      t.date :trip_date
      t.string :holiday
      t.integer :fixed
      t.integer :fixed_over
      t.integer :moving_allowance

      t.timestamps
    end
  end
end
