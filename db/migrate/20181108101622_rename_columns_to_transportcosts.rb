class RenameColumnsToTransportcosts < ActiveRecord::Migration[5.1]
  def change
    rename_column :transport_costs, :exclusion_type, :etc
  end
end
