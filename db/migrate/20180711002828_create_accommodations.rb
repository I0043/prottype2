class CreateAccommodations < ActiveRecord::Migration[5.1]
  def change
    create_table :accommodations do |t|
      t.references :report, foreign_key: true
      t.date :checkin
      t.date :checkout
      t.string :city_name
      t.integer :accommodation
      t.integer :add_money

      t.timestamps
    end
  end
end
