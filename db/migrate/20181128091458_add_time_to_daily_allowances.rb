class AddTimeToDailyAllowances < ActiveRecord::Migration[5.1]
  def change
    add_column :daily_allowances, :moving_type, :string
    add_column :daily_allowances, :start_time, :time
    add_column :daily_allowances, :finish_time, :time
  end
end
