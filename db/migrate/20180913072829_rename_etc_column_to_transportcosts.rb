class RenameEtcColumnToTransportcosts < ActiveRecord::Migration[5.1]
  def change
      rename_column :transport_costs, :ETC, :exclusion_type
  end
end
