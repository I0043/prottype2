class CreateTransportCosts < ActiveRecord::Migration[5.1]
  def change
    create_table :transport_costs do |t|
      t.references :report, foreign_key: true
      t.date :move_date
      t.string :dept
      t.string :dest
      t.string :root
      t.string :access
      t.string :ETC
      t.string :drive_type
      t.integer :transport_cost

      t.timestamps
    end
  end
end
