class CreateCalendars < ActiveRecord::Migration[5.1]
  def change
    create_table :calendars do |t|
      t.date :work_date
      t.integer :holiday

      t.timestamps
    end
  end
end
