Rails.application.routes.draw do
  resources :transfers
  resources :grade_fixeds
  resources :grades
  resources :fixeds
  resources :etc_lists
  resources :access_types
  resources :calendars
  resources :gradelists
  resources :city_names
  
  resources :orders
  # resources :orders, only: [:index] do
  #   collection do
  #     get :search
  #   end
  # end
  
  resources :users
  # resources :others
  # resources :daily_allowances
  # resources :accommodations
  # resources :transport_costs
  # resources :reports
  resources :reports do
    resources :others
    resources :daily_costs
    # resources :daily_allowances
    # resources :additionals
    # resources :accommodations
    resources :transport_costs
    # resources :moving_times
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
